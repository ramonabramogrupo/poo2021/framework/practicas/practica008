<?php
    use yii\helpers\Html;
?>
<div class="row">
    <div class="col-lg-5">
        <?= Html::img("@web/imgs/portada.png",["class"=>'img-fluid']) ?>
    </div>
    <div class="col-lg-7">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid, cupiditate, ducimus, laudantium deserunt odit error excepturi consequuntur sit voluptatibus minima pariatur sed eius dolorem! Fugit reiciendis repellat ab dolore libero?
    </div>
</div>
