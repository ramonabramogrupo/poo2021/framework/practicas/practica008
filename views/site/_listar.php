<?php
    use yii\helpers\Html;
    use app\widgets\Modal;
?>
<div class="row">
    <h2 class="col-lg-2">
        <?= $model->id ?>
    </h2>
    
    <div class="ml-1 col-lg-8 pt-2">
        <div class="bg-white border p-3">
            <?= Html::encode(substr($model->descripcion,0,100)) . "..."?>
        </div>
        <div class="mt-2 float-right">
            <?= Html::a('<i class="fas fa-plus-circle fa-2x"></i>',['site/ver','id'=>$model->id],['class'=>'botonModal']) ?>
        </div>
    </div>
    
    
</div>

<?php
    echo Modal::widget([
        "boton" =>"botonModal"
    ]);
?>

