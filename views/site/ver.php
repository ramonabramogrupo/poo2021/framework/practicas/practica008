<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
    "model" => $model,
    'attributes' =>[
            'id',
            'descripcion',
            'procesador',
            'memoria',
            'discoduro',
            [
                'attribute' => 'ethernet',
                'format' => 'raw',
                'value' => function($model){
                    
                    // Colocar un checkbox deshabilitado
                    //return Html::activeCheckbox($model, 'ethernet',['disabled'=>true]);
                    
                    if($model->ethernet){
                        return '<i class="fas fa-check-square"></i>';
                    }else{
                        return '<i class="fas fa-times"></i>';
                    }
                }
            ],
            [
                'attribute' => 'wifi',
                'format' => 'raw',
                'value' => function($model){
                    
                    // Colocar un checkbox deshabilitado
                    //return Html::activeCheckbox($model, 'wifi',['disabled'=>true]);
                    
                    if($model->wifi){
                        return '<i class="fas fa-check-square"></i>';
                    }else{
                        return '<i class="fas fa-times"></i>';
                    }
                }
            ],
            'video',
        ]
]);

echo Html::a("Ver por bloques",
        ['site/verbloques',"id"=>$model->id],
        ["class"=>"btn btn-primary"]);



