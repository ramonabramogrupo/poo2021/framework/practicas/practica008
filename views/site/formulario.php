<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\Ordenadores */
/* @var $form ActiveForm */
?>
<h2 class="bg-secondary p-2 mb-3 text-white">
    <?= $titulo ?>
</h2>
<div class="site-formulario">
    <?php 
        $opciones=[
            'template' => '{label}{input}',
            'inputOptions' => ['class'=>'col-lg-9'],
            'labelOptions' => ['class'=>'col-lg-2'],
            'options' =>["class"=>'row mb-2']
        ];
        
        $opciones1=[
            'template' => '{label}{input}',
            'inputOptions' => ['class'=>'col-lg-9 p-0'],
            'labelOptions' => ['class'=>'col-lg-1 p-0'],
            'options' => ['form-check form-inline']
        ];
        
    ?>
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'descripcion',$opciones)->textarea(["rows"=>8]) ?>
        <?= $form->field($model, 'procesador',$opciones) ?>
        <?= $form->field($model, 'memoria',$opciones) ?>
        <?= $form->field($model, 'discoduro',$opciones) ?>
        <?= $form->field($model, 'video',$opciones) ?>
        
        <?php 
            // con el checkbox como switch
            echo $form->field($model, 'ethernet')
            ->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                    'onText' => 'Con Ethernet',
                    'offText' => 'Sin Ethernet',
                ]   
            ])->label(''); // quito el label del control 
        ?>
        
        <?php 
            // con el checkbox normal
            /*
            echo $form->field($model, 'ethernet',$opciones1)
            ->checkbox(['class'=>''],false); 
             */
        ?>
    
        <?php 
            /*echo $form->field($model, 'wifi',$opciones1)
            ->checkbox(['class'=>''],false); */ 
        ?>
    
        <?php 
            // con el checkbox como switch
            echo $form->field($model, 'wifi')
            ->widget(SwitchInput::classname(), [
                'pluginOptions' => [
                    'onText' => 'Con Wifi',
                    'offText' => 'Sin Wifi',
                ]   
            ])->label(''); // quito el label del control 
        ?>
    
        <div class="form-group">
            <?= Html::submitButton($boton, ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario -->
