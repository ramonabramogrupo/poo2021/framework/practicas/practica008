<?php
use yii\grid\GridView;
use yii\helpers\Html;
?>

<div class="row">
    <?= GridView::widget([
        "dataProvider" => $dataProvider,
        'columns' =>[
            'id',
            'descripcion',
            'procesador',
            'memoria',
            'discoduro',
            [
                'attribute' => 'ethernet',
                'format' => 'raw',
                'value' => function($model){
                    
                    // Colocar un checkbox deshabilitado
                    //return Html::activeCheckbox($model, 'ethernet',['disabled'=>true]);
                    
                    if($model->ethernet){
                        return '<i class="fas fa-check-square"></i>';
                    }else{
                        return '<i class="fas fa-times"></i>';
                    }
                }
            ],
            [
                'attribute' => 'wifi',
                'format' => 'raw',
                'value' => function($model){
                    
                    // Colocar un checkbox deshabilitado
                    //return Html::activeCheckbox($model, 'wifi',['disabled'=>true]);
                    
                    if($model->wifi){
                        return '<i class="fas fa-check-square"></i>';
                    }else{
                        return '<i class="fas fa-times"></i>';
                    }
                }
            ],
            'video',
            [
             'header' => 'Acciones',
             'headerOptions' => ["class" => "btn-link font-weight-bold"],
             'class' => 'yii\grid\ActionColumn',  
             'template' => '{insertar} {ver} {update} {delete}',
             'buttons' => [
                 'insertar' => function ($ruta,$modelo,$indice){
                        return Html::a(
                                '<i class="fas fa-plus-square"></i>',
                                ["site/crear"],['class'=>'botonModal']);
                 }, 
                 'ver' => function ($ruta,$modelo,$indice){
                        return Html::a(
                                '<i class="fas fa-eye"></i>',
                                ["site/ver","id"=>$modelo->id]);
                 } ,
                 // accion eliminar 
                 /*'eliminar' => function($url, $model){
                    return Html::a('eliminar', ['site/eliminar', 'id' => $model->id], [
                        'data' => [
                                'confirm' => '¿Seguro que quieres eliminarlo?',
                                'method' => 'post',
                        ],
                    ]);
                }*/
                         
                ]
            ]
        ]
    ]);?>
</div>

<div class="row">
    <?= 
        Html::a(
            "Crear Ordenador",
            ["site/crear"],
            ["class" => "btn btn-primary"]);
    ?>
</div>