<?php

use \yii\widgets\ListView;


echo ListView::widget([
    "dataProvider" => $dataProvider,
    "itemView" => "_listar",
    "options" => [
        "class" => "row"
    ],
    "itemOptions" =>[
        "class" => "col-lg-5 border p-4 m-2",
        "style" => "background-color:#eee",
    ],
    "summaryOptions" =>[
        "class"=> "col-lg-12",
    ]
]);

