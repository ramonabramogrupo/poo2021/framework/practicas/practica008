<?php
use yii\bootstrap4\Accordion;
use yii\helpers\Html;

// ------
// METODO 1 
// coloco todos los elementos a mano

    // preparar para la informacion de red
    if($model->ethernet){
        $ethernet='<i class="fas fa-check-square"></i>';
    }else{
        $ethernet='<i class="fas fa-times"></i>';
    }
    
    if($model->wifi){
        $wifi='<i class="fas fa-check-square"></i>';
    }else{
        $wifi='<i class="fas fa-times"></i>';
    }
    // fin de preparar para la informacion de red
    
    echo Accordion::widget([
    'items' => [
        [
            'label' => $model->getAttributeLabel('descripcion') ,
            'content' =>$model->descripcion,
        ],
        [
            'label' =>$model->getAttributeLabel('procesador') ,
            'content' =>$model->procesador,
        ],
        [
            'label' =>$model->getAttributeLabel('memoria') ,
            'content' =>$model->memoria,
        ],
        [
            'label' => 'Red',
            'content' => $ethernet . ' Ethernet <br>' . $wifi . ' Wifi'
        ],
        [
            'label' =>$model->getAttributeLabel('video') ,
            'content' =>$model->video,
        ],
    ]
]);
    
// FIN DEL METODO1

// -----------------------------------------
// METODO 2 
// UTILIZANDO UN ARRAY

// creo un array con todos los elementos a mostrar
/*   $elementos=[];
    
    foreach ($model as $etiquetaCampo=>$valorCampo) {
        if($etiquetaCampo=='id' || $etiquetaCampo=='discoduro'){
            continue;
        }else{
            $elementos[]=[
                "label"=> $etiquetaCampo,
                "content"=>$valorCampo
            ];
        }
    }
    
    
    if($model->ethernet){
        $ethernet='<i class="fas fa-check-square"></i>';
    }else{
        $ethernet='<i class="fas fa-times"></i>';
    }
    
    if($model->wifi){
        $wifi='<i class="fas fa-check-square"></i>';
    }else{
        $wifi='<i class="fas fa-times"></i>';
    }
    
    $elementos[]=[
        'label' => 'Red',
        'content'=> $ethernet . ' Ethernet <br>' . $wifi . ' Wifi'
    ];
        
    
    echo Accordion::widget([
        'items' => $elementos,
        
    ]);
    
// FIN DEL METODO 2
*/

echo Html::a("Ver Normal",
        ['site/ver',"id"=>$model->id],
        ["class"=>"btn btn-primary mt-2"]);